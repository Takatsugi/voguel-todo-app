import { Component, OnInit } from '@angular/core';
import { Todos } from 'src/app/models/todos.model';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  pageInit: number = 0;
  pageEnd: number = 10;
  todos!: Todos[];
  todosClone!: Todos[];
  checkedSearch: string;
  userId: boolean = false;
  id: boolean = false;
  title: boolean = false;
  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.taskService.getTodos().subscribe((todos: Todos[]) => {
      this.todos = todos;
      this.todosClone = todos;
    })
  }

  onNext(){
    if (this.todosClone.length > this.pageEnd){
      this.pageInit+=10;
      this.pageEnd+=10;
    }
    
    }

  onPrec(){
    if (this.pageInit!==0){
      this.pageInit-=10;
      this.pageEnd-=10;
    }
    
  }

  onClickDelete(data) {
    
      this.todosClone = this.todos.filter(val => val.id !== data.id);
      this.todos= this.todosClone;
    
  }

  onClickSearch(data){
    this.todosClone = this.todos
    //if(data.length!==0)
    if(this.userId){
    this.todosClone = this.todosClone.filter((val) => {
      return val.userId.toString().toLowerCase()===data.toString().toLowerCase();
  })
}
    
    
     if(this.title){
      this.todosClone = this.todosClone.filter((val) => {
        return val.title.toLowerCase().includes(data.toLowerCase());
      });
    }

     if(this.id){
      this.todosClone = this.todosClone.filter((val) => {
        return val.id.toString().toLowerCase()===data.toString().toLowerCase();
      });
    }
    
    
    //console.log("aaa", val.userId)
  }

  changeStatus(data,e) {
    if (data==="UserId")
    this.userId= !this.userId;
    if(data=== "Id")
    this.id= !this.id;
    if(data=== "Title")
    this.title= !this.title
  }
}
