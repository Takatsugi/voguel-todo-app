import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor( private webRequest: WebRequestService) { }

  getTodos() {
    return this.webRequest.get('todos');
  }

}
