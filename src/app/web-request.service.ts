import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WebRequestService {
  readonly ROOT_URL;

  constructor( private http: HttpClient) {
    this.ROOT_URL = 'https://jsonplaceholder.typicode.com'
   }

   get(uri: string) {
    return this.http.get(`${this.ROOT_URL}/${uri}`);
  }
}